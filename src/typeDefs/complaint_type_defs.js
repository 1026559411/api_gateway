const { gql } = require('apollo-server');

const complaintTypeDefs = gql `

    type Complaint {
        id: String!
        username: String!
        description: String!
        date: String!
    }

    input ComplaintInput {
        username: String!
        description: String!
        date: String!
    }
    
    extend type Query {
        complaintById(id: String!): Complaint
        complaintsByUsername(username: String!): [Complaint]
    }

    extend type Mutation {
        createComplaint(complaint: ComplaintInput!): Complaint
    } 
`;

module.exports = complaintTypeDefs;