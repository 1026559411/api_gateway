//Se llama al typedef (esquema) de cada submodulo
const complaintTypeDefs = require('./complaint_type_defs');
const authTypeDefs = require('./auth_type_defs');

//Se unen
const schemasArrays = [authTypeDefs, complaintTypeDefs];

//Se exportan
module.exports = schemasArrays;