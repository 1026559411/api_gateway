const complaintResolver = {
    Query: {
        complaintsByUsername: async(_, { username }, { dataSources, userIdToken }) => {
            usernameToken = (await dataSources.authAPI.getUser(userIdToken)).username
            if (username == usernameToken)
                return dataSources.complaintAPI.complaintsByUsername(username)
            else
                return null
        },
        complaintById: async(_, { id }, { dataSources, userIdToken }) => {
            usernameToken = (await dataSources.authAPI.getUser(userIdToken)).username
            if (1)
                return dataSources.complaintAPI.complaintById(id)
            else
                return null
        }
    },
    Mutation: {
        createComplaint: async(_, { complaint }, { dataSources, userIdToken }) => {
            usernameToken = (await dataSources.authAPI.getUser(userIdToken)).username
            if (complaint.username == usernameToken)
                return dataSources.complaintAPI.createComplaint(complaint)
            else
                return null
        }

    }
};


module.exports = complaintResolver;