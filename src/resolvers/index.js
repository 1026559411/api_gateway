const complaintResolver = require('./complaint_resolver');
const authResolver = require('./auth_resolver');

const lodash = require('lodash');

const resolvers = lodash.merge(complaintResolver, authResolver);

module.exports = resolvers;