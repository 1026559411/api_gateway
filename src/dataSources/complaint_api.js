const { RESTDataSource } = require('apollo-datasource-rest');

const serverConfig = require('../server');

class ComplaintAPI extends RESTDataSource {

    constructor() {
        super();
        this.baseURL = serverConfig.complaint_api_url;
    }

    async createComplaint(complaint) {
        complaint = new Object(JSON.parse(JSON.stringify(complaint)));
        return await this.post('/complaints', complaint);
    }

    async complaintsByUsername(username) {
        return await this.get(`/mycomplaints/${username}`);
    }

    async complaintById(id) {
        return await this.get(`/complaints/${id}`);
    }
}

module.exports = ComplaintAPI;